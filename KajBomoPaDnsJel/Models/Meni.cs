﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KajBomoPaDnsJel.Models
{
    public class Meni
    {
        public Meni(string ImeMenija)
        {
            this.Ime = ImeMenija;
            this.IsFacebookPost = false;
            this.FacebookPostUrl = "";
        }
        public Meni(bool FacebookPost, string FacebookPostUrl)
        {
            this.Ime = "";
            this.IsFacebookPost = FacebookPost;
            this.FacebookPostUrl = FacebookPostUrl;
        }

        public string Ime { get; set; }
        public bool IsFacebookPost { get; set; }
        public string FacebookPostUrl { get; set; }
    }
}