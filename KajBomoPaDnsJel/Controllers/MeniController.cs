﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using KajBomoPaDnsJel.Models;
using static KajBomoPaDnsJel.BusinessLogic.MeniWebPageParser;

namespace KajBomoPaDnsJel.Controllers
{
    public class MeniController : Controller
    {
        // GET: Meni
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Restavracija(int id)
        {
            WebPagesEnum restavracija;
            List<Meni> meniji = new List<Meni>();
            if (Enum.TryParse(id.ToString(), out restavracija))
            {
                ViewBag.restavracija = urlsWebPagesEnum[restavracija].FullName;
                meniji = GetMenuFromWebPage(restavracija);
            }
            else
            {
                // ni pravi Id restavracije
            }

            //meniji.Add(new Meni("tola jed"));

            return View(meniji);
        }

        public ActionResult SendMenus(string key_mails)
        {
            bool mailsSend = false;

            string[] key_mails_splitted = key_mails.Split(';');
            if (key_mails_splitted.Length == 2)
            {
                string key = key_mails_splitted[0];
                if (key != "5a0600ee-3a99-4d36-af1e-426f1d196d80")  // "5985cd81-d6c9-4fec-949b-602bfd12fe12"
                {
                    //return HttpNotFound("Failed call.");
                    ViewBag.notification = "Unvalid call!";
                    return View();
                }

                List<string> toMails = new List<string>();
                //toMails.Add("ales.koncilja@gmail.com");
                toMails.AddRange(key_mails_splitted[1].Split(',').ToList());

                StringBuilder sb_menu = new StringBuilder();
                List<Meni> meniji = new List<Meni>();
                bool obstajaVsajEnMeni = false;
                foreach (WebPagesEnum restavracija in urlsWebPagesEnum.Keys)
                {
                    //if (restavracija != WebPagesEnum.FlorjanckovHram)
                    //    continue;

                    meniji = GetMenuFromWebPage(restavracija);

                    // Element, Farao, Rogovilc, FlorjanckovHram
                    if (meniji.Count > 0)
                    {
                        obstajaVsajEnMeni = true;

                        sb_menu.AppendFormat("<h1>{0}:</h1>", urlsWebPagesEnum[restavracija].FullName);

                        sb_menu.Append("<ul>");
                        foreach (Meni m in meniji)
                        {
                            if (m.IsFacebookPost)
                            {
                                sb_menu.AppendFormat("<li><a href=\"{0}\">{1} menu</a></li>", m.FacebookPostUrl, urlsWebPagesEnum[restavracija].FullName);
                            }
                            else
                            {
                                sb_menu.AppendFormat("<li>{0}</li>", m.Ime);
                            }
                        }
                        sb_menu.Append("</ul>");
                        sb_menu.Append("<br /><br />");
                    }
                }

                if (!obstajaVsajEnMeni)
                {
                    sb_menu.AppendLine("Trenutno ni menijev!");
                }
                else
                {
                    sb_menu.AppendLine("Dober tek!");
                }


                sb_menu.Append("<br /><br /><br />");
                sb_menu.AppendLine(string.Format("Prenesi mobilno aplikacijo <a target=\"_blank\" title=\"Prenesi mobilno aplikacijo\" " +
                    "href=\"https://gonative.io/share/qoaped\"><i>Kaj bomo pa dns jel</i></a>."));

                sb_menu.Append("<br /><br />");
                sb_menu.AppendLine(string.Format("Obišči spletno stran <a href=\"https://kajbomopadnsjel.azurewebsites.net\">kajbomopadnsjel</a>."));
                sb_menu.Append("<br /><br />");

                sb_menu.Append("<img src=\"https://kajbomopadnsjel.azurewebsites.net/Images/dober-tek.jpg\" alt=\"Dober tek\">");
                try
                {
                    mailsSend = SendEmail(toMails, string.Format("Kaj bomo pa dns jel: {0}", DateTime.Now.ToString("dd.MM.yyyy")), sb_menu.ToString());
                    ViewBag.notification = mailsSend ? "Menuji poslani!" : "Pošiljanje mailov ni uspelo!";
                }
                catch (Exception e)
                {
                    ViewBag.notification += e.Message + "  " + e.ToString();
                }
            }
            else
            {
                return HttpNotFound("Failed call.");
            }
            return View();
        }

        // GET: Meni/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Meni/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Meni/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Meni/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Meni/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Meni/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Meni/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
