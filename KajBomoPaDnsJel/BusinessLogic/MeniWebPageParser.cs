﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using HtmlAgilityPack;
using Fizzler.Systems.HtmlAgilityPack;
using System.Text;
using KajBomoPaDnsJel.Models;
using System.Net;
using System.IO;
using SimpleFacebookClient;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.Net.Mail;

namespace KajBomoPaDnsJel.BusinessLogic
{
    public static class MeniWebPageParser
    {
        public enum WebPagesEnum
        {
            Element, Farao, Rogovilc, FlorjanckovHram
        };
        public class WebPageEnumDetails
        {
            public WebPageEnumDetails(string Url, string FullName)
            {
                this.Url = Url;
                this.FullName = FullName;
            }

            public string Url { get; set; }
            public string FullName { get; set; }
        }
        public static Dictionary<WebPagesEnum, WebPageEnumDetails> urlsWebPagesEnum = new Dictionary<WebPagesEnum, WebPageEnumDetails>()
        {
            { WebPagesEnum.Element, new WebPageEnumDetails("http://www.kaval-group.si/ELEMENT,,ponudba,kosila.htm", "Element") },
            { WebPagesEnum.Rogovilc, new WebPageEnumDetails("https://www.facebook.com/Rogovilc/", "Rogovilc") },
            { WebPagesEnum.Farao, new WebPageEnumDetails("http://www.farao.si/meni.php", "Farao") },
            { WebPagesEnum.FlorjanckovHram, new WebPageEnumDetails("https://www.facebook.com/gostilna.florjanckovhram/", "Florjančkov hram") }
        };

        public static List<Meni> GetMenuFromWebPage(WebPagesEnum webPage)
        {
            string url = urlsWebPagesEnum[webPage].Url;

            //get the page
            var web = new HtmlWeb();
            HtmlDocument document;
            HtmlNode page;

            string datePatetrn_1 = "dd.MM.yyyy";
            string datePatetrn_2 = "d.M.yyyy";
            string todayDate_1 = DateTime.Now.Date.ToString(datePatetrn_1);
            string todayDate_2 = DateTime.Now.Date.ToString(datePatetrn_2);

            List<Meni> meni_list = new List<Meni>();

            switch (webPage)
            {
                case WebPagesEnum.Element:
                    {
                        try
                        {
                            web.OverrideEncoding = Encoding.GetEncoding(1250);
                            document = web.Load(url);
                            page = document.DocumentNode;

                            // da dobim kateri je izbran za danes
                            List<string> classes = page.QuerySelector("#kosilaMeni").QuerySelectorAll("li.childNaviLiElement a")
                                                    .Where(a => a.InnerText.Contains(todayDate_1) || a.InnerText.Contains(todayDate_2)).FirstOrDefault().GetClasses().ToList();
                            string naviLinkId_str = classes.Where(c => c.Contains("navi-")).FirstOrDefault().Replace("navi-", "");

                            string meni_str = "";
                            HtmlNode meni;
                            List<HtmlNode> meniNodes_p = null;
                            // dobim vse kar je na današnjem meniju
                            HtmlNode meniNode = page.QuerySelectorAll("#kosilaShow div.show-" + naviLinkId_str).FirstOrDefault();
                            if (meniNode != null)
                            {
                                if (meniNode.QuerySelectorAll("p").Count() == 1)
                                {
                                    /*List<string> meniji = HttpUtility.HtmlDecode(meniNode.QuerySelector("p").InnerText).Replace("***", ";").Split(';').ToList();
                                    foreach(var m in meniji)
                                    {
                                        if (!string.IsNullOrEmpty(m.Trim()))
                                            meni_list.Add(new Meni(string.Format("{0}", m)));
                                    }*/
                                    meniNodes_p = meniNode.QuerySelector("p").ChildNodes.Where(ch => HttpUtility.HtmlDecode(ch.InnerText).Trim().Length > 0 && !ch.InnerText.Contains("*")).ToList();   //  .Where(p => p.InnerText.Replace("*", "").Length > 0).ToList();

                                }
                                else
                                {
                                    //meniNodes_p = meniNode.QuerySelectorAll("p").Where(p => p.InnerText.Replace("*", "").Length > 0).ToList();
                                    meniNodes_p = meniNode.QuerySelectorAll("p").Where(p => !p.InnerText.Contains("*")).ToList();

                                    //StringBuilder meniji = new StringBuilder();
                                    //byte[] bytes;
                                    //bytes = Encoding.Default.GetBytes(meni.InnerText);
                                    //meniji.Add(Encoding.UTF8.GetString(bytes));
                                    //meniji.AppendFormat("{0}{1}<br />", (meni.InnerText.Trim().StartsWith("(") ? "- " : ""), meni.InnerText);

                                    //Encoding srcEncoding = Encoding.GetEncoding("Windows-1250");    //.GetString(bytes);
                                    //UnicodeEncoding dstEncoding = new UnicodeEncoding();
                                    //byte[] srcBytes = srcEncoding.GetBytes(meni.InnerText);
                                    //byte[] dstBytes = dstEncoding.GetBytes(meni.InnerText);
                                    //var str = dstEncoding.GetString(dstBytes);

                                    //Encoding wind1252 = Encoding.GetEncoding(1250);
                                    //Encoding utf8 = Encoding.UTF8;
                                    //byte[] wind1252Bytes = wind1252.GetBytes(meni.InnerText);
                                    //byte[] utf8Bytes = Encoding.Convert(wind1252, utf8, wind1252Bytes);
                                    //string utf8String = Encoding.UTF8.GetString(utf8Bytes);
                                    /*for (int i = 0; i < meniNodes_p.Count; i++)
                                    {
                                        meni = meniNodes_p[i];
                                        meni_str = HttpUtility.HtmlDecode(meni.InnerText).Trim();   //  meni.InnerText.Replace("&scaron;", "š").Trim();


                                        if (i + 1 < meniNodes_p.Count && meniNodes_p[i + 1].InnerText.StartsWith("("))
                                        {
                                            //meni_str += " " + meniNodes_p[i + 1].InnerText.Replace("&scaron;", "š").Trim();
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (meni_str.EndsWith(":") && i + 1 < meniNodes_p.Count)
                                        {
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (!string.IsNullOrEmpty(meni_str))
                                            meni_list.Add(new Meni(string.Format("{0}", meni_str)));
                                    }*/
                                }
                                if (meniNodes_p != null)
                                {
                                    for (int i = 0; i < meniNodes_p.Count; i++)
                                    {
                                        meni = meniNodes_p[i];
                                        meni_str = HttpUtility.HtmlDecode(meni.InnerText).Replace("*", "").Trim();

                                        if (i + 1 < meniNodes_p.Count && meniNodes_p[i + 1].Name.Equals("strong"))
                                        {
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (meni_str.EndsWith(":") && i + 1 < meniNodes_p.Count)
                                        {
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (i + 1 < meniNodes_p.Count && HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim().StartsWith("("))
                                        {
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (i + 1 < meniNodes_p.Count && HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim().Contains("€"))
                                        {
                                            meni_str += " " + HttpUtility.HtmlDecode(meniNodes_p[i + 1].InnerText).Trim();
                                            i++;
                                        }

                                        if (!string.IsNullOrEmpty(meni_str))
                                            meni_list.Add(new Meni(string.Format("{0}", meni_str)));
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                    break;

                case WebPagesEnum.Farao:
                    {
                        try
                        {
                            web.OverrideEncoding = Encoding.UTF8;   //  GetEncoding(1250);
                            document = web.Load(url);
                            page = document.DocumentNode;
                            string meni_str;
                            string inner_text = "";

                            HtmlNode praviP = page.QuerySelectorAll("#scroll p").Where(p => p.InnerText.Contains(todayDate_1)).FirstOrDefault();

                            StringBuilder sb = new StringBuilder();
                            if (praviP != null)
                            {
                                bool naselDanasnjiMeni = false;
                                //bool konecMenija = false;

                                foreach (var cn_p in praviP.ChildNodes)
                                {
                                    //if (konecMenija)
                                    //    break;

                                    inner_text = HttpUtility.HtmlDecode(cn_p.InnerText).Trim();

                                    if (inner_text.Contains(todayDate_1))
                                    {
                                        naselDanasnjiMeni = true;
                                        continue;
                                    }
                                    else if (naselDanasnjiMeni && inner_text.Contains("FARAO:"))
                                    {
                                        //if (naselDanasnjiMeni)
                                        //{
                                        // sem zaključil z današnjim menijem in ni potrebno nadaljevati
                                        naselDanasnjiMeni = false;
                                        //konecMenija = true;
                                        break;
                                        //}
                                    }

                                    if (naselDanasnjiMeni)
                                    {
                                        inner_text = inner_text.Replace("€", "€;"); // .Replace("\r", "").Replace("\n", "").Trim().Replace("+", "-");
                                        if (!string.IsNullOrEmpty(inner_text))
                                            sb.Append(inner_text);
                                    }
                                }
                            }

                            if (sb.Length > 0)
                            {
                                meni_str = sb.ToString();
                                string[] splittedMeni_str = meni_str.Split(';');
                                foreach (string meni in splittedMeni_str)
                                {
                                    if (!string.IsNullOrEmpty(meni.Trim()))
                                        meni_list.Add(new Meni(meni.Trim()));
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                    break;

                case WebPagesEnum.Rogovilc:
                    {
                        try
                        {
                            document = web.Load(url);
                            page = document.DocumentNode;
                            string meni_str;
                            string lastPost = page.QuerySelector("div.text_exposed_root p").InnerHtml;
                            // "Sreda, 20.12.2017"
                            if (lastPost.Contains(todayDate_1) && lastPost.Contains("MALICE"))
                            {
                                string[] splittedLastPost = lastPost.Replace("<br>", ";").Split(';');

                                foreach (string line in splittedLastPost)
                                {
                                    if (line.Contains(todayDate_1) || line.Contains("MALICE OD"))
                                    {
                                        meni_str = "";
                                    }
                                    else if (line.Contains("<span"))
                                    {
                                        meni_str = line.Remove(line.IndexOf("<span")).Trim();
                                        //meni_list.Add(new Meni(meni_str));
                                    }
                                    else if (line.Contains("</span"))
                                    {
                                        meni_str = line.Remove(line.IndexOf("</span")).Trim();
                                        //meni_list.Add(new Meni(meni_str.Trim()));
                                    }
                                    else
                                    {
                                        //meni_list.Add(new Meni(line.Trim()));
                                        meni_str = line.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(meni_str))
                                        meni_list.Add(new Meni(meni_str));
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                    break;
                case WebPagesEnum.FlorjanckovHram:
                    {
                        try
                        {
                            document = web.Load(url);
                            page = document.DocumentNode;
                            var last_post = page.QuerySelector("div.userContentWrapper");

                            //var lastPostedImage = page.QuerySelector("img.scaledImageFitWidth.img");
                            //string lastPostedImage_url = lastPostedImage.Attributes["src"].Value;
                            //meni_list.Add(new Meni(lastPostedImage_url));
                            //var img_html = String.Format("<img src=\"{0}\" alt=\"Florjančkov hram menu\">", meni_img_url);

                            string lastPostedImage_url2 = "https://www.facebook.com" + last_post.QuerySelector("a[rel='theater']").Attributes["href"].Value;
                            meni_list.Add(new Meni(true, lastPostedImage_url2));  // v redu je sicer tudi ta - url je slika, ne post

                            //document = web.Load(lastPostedImage_url2);
                            //page = document.DocumentNode;
                            //string lastPostedImage_postUrl2 = page.QuerySelector("meta[property='og:url']").Attributes["content"].Value;

                            //meni_list.Add(new Meni(true, lastPostedImage_postUrl2));  //  last post

                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                    break;
                default:
                    {
                    }
                    //return "";
                    break;
            }

            //loop through all div tags with item css class
            //foreach (var item in page.QuerySelectorAll("div.item"))
            //{
            //    var title = item.QuerySelector("h3:not(.share)").InnerText;
            //    var date = DateTime.Parse(item.QuerySelector("span:eq(2)").InnerText);
            //    var description = item.QuerySelector("span:has(b)").InnerHtml;
            //}

            return meni_list;
        }


        public static bool SendEmail(List<string> toMails, string subject, string body)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();

                MailMessage message = new MailMessage();
                foreach (string to in toMails)
                {
                    message.To.Add(to);
                }
                //message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;

                string plainText = System.Text.RegularExpressions.Regex.Replace(message.Body, @"<[^>]*>", string.Empty);
                plainText = plainText.Replace("&nbsp;", " ");

                AlternateView plainView = AlternateView.CreateAlternateViewFromString(plainText, Encoding.UTF8, "text/plain");
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString("<html><body>" + message.Body + "</body></html>", System.Text.Encoding.UTF8, "text/html");

                plainView.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
                htmlView.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;

                message.AlternateViews.Add(plainView);
                message.AlternateViews.Add(htmlView);

                smtp.Send(message);

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message + "  " + e.ToString());
                throw e;

                return false;
            }
        }
    }
}






// https://developers.facebook.com/apps/1747316255578177/dashboard/
// https://developers.facebook.com/apps/1747316255578177/review-status/
// https://smashballoon.com/custom-facebook-feed/access-token/
// https://www.nuget.org/packages/SimpleFacebookClient
// https://stackoverflow.com/questions/30487566/trying-to-get-last-post-of-a-page-using-facebook-sdk-c-sharp
/*

    // App ID
    // 1747316255578177
    //App Secret
    //f8d2e1112fd9436663bbabf769cf8c7b
    // Your Access Token is 1747316255578177|yvzUwMwapdJb7ONsw88ohGinvUI
    // Client Token
    //955f30080709996f435882e83ce6c2b1
    FacebookClient client = new FacebookClient("955f30080709996f435882e83ce6c2b1");
    client = new FacebookClient("1747316255578177");
    client = new FacebookClient("yvzUwMwapdJb7ONsw88ohGinvUI");
    client = new FacebookClient("1747316255578177|yvzUwMwapdJb7ONsw88ohGinvUI");

    // Get my feed
    dynamic results = client.Get<dynamic>("/me/feed");
    ArrayList posts = new ArrayList();

    // Loop through the results
    while (results["data"].Count > 0)
    {
        foreach (var item in results["data"])
        {
            var metadata = new Dictionary<string, string>()
            {
                { "Story", Convert.ToString(item["story"]) },
                { "Type", Convert.ToString(item["type"]) },
                { "Picture", Convert.ToString(item["picture"])},
                { "Link", Convert.ToString(item["link"]) },
                { "Name", Convert.ToString(item["name"]) },
                { "Description", Convert.ToString(item["description"]) },
                { "Status Type", Convert.ToString(item["status_type"]) }};

            posts.Add(new
            {
                Id = Convert.ToString(item["id"]),
                PostBody = Convert.ToString(item["message"]),
                CreationTime = Convert.ToString(item["created_time"]),
                PostWriter = Convert.ToString(item["from.name"]),
                Metadata = metadata
            });
        }

        // Manage paging
        var incomingParameters = GetQueryStringParameters(results["paging"]["next"].ToString());

        string until = incomingParameters["until"];
        string pagingToken = incomingParameters["__paging_token"];

        Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                { "until", until },
                { "__paging_token", pagingToken }
            };

        results = client.Get<dynamic>("me/feed", parameters);

        if (results["error"] != null)
        {
            Thread.Sleep(1000 * 60);
        }
    }
    var app_id = "1747316255578177";
    var secret_id = "f8d2e1112fd9436663bbabf769cf8c7b";
    Dictionary<string, string> tokens = new Dictionary<string, string>();
    url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&scope={2}&code={3}&client_secret={4}&perms=status_update"
, app_id, "http://localhost:53394/", "scope", "si", secret_id);
    //url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}client_secret={1}&perms=status_update"
    //    , "1747316255578177", "f8d2e1112fd9436663bbabf769cf8c7b");
    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
    {

        StreamReader rd = new StreamReader(response.GetResponseStream());

        string vals = rd.ReadToEnd();
        foreach (string token in vals.Split('&'))
        {
            tokens.Add(token.Substring(0, token.IndexOf('=')), token.Substring(token.IndexOf("=") + 1, token.Length - token.IndexOf('=') - 1));
        }
        string access_token = tokens["access_token"];
        //Session["access_token"] = access_token;

        var client = new FacebookClient(access_token);


        dynamic mywallPosts = client.Get<dynamic>("/me/posts");
        var posti = mywallPosts.data;
        //List<Posts> fb_posts = new List<Posts>();

        //for (int i = 0; i < mywallPosts.data.Count; i++)
        //{

        //    Posts post = new Posts();
        //    post.PostId = mywallPosts.data[i].id;
        //    post.PostMessage = mywallPosts.data[i].message;
        //    post.PostPicture = mywallPosts.data[i].picture;
        //    post.PostStory = mywallPosts.data[i].story;
        //    post.UserName = mywallPosts.data[i].from.name;

        //    fb_posts.Add(post);
        //}
    }*/
